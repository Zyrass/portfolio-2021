import './App.css';
import Home from './components/pages/Home/Home';
import { Footer, Navbar } from './utils'

function App() {
  return (
    <div className="App">
      <Home />
      <Footer />
    </div>
  );
}

export default App;
