import "./Navbar.scss"

const Navbar = () => {
    return(
        <nav>
            <span>Alain Guillon </span>
            <ul>
                <li><a href="">Accueil</a></li>
                <li><a href="">Projects</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">A Propos</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </nav>
    )
}

export default Navbar